import json

names = ['acinetobacter_baumannii',
 'bacillus_cereus',
 'burkholderia_pseudomallei',
 'clostridioides_difficile',
 'enterococcus_faecalis',
 'enterococcus_faecium',
 'escherichia_coli',
 'klebsiella_pneumoniae',
 'listeria_monocytogenes',
 'mycobacterium_abscessus',
 'mycobacterium_tuberculosis',
 'neisseria_gonorrhoeae',
 'neisseria_meningitidis',
 'pseudomonas_aeruginosa',
 'salmonella_enterica',
 'streptococcus_pneumoniae']

def get_bacterial_clusters():
    with open('bacterial_clusters.json') as f:
        return json.load(f)
    
DNA_damage_genes = (['lexA','recA','umuC','polB','umuD','dinB','uvr']+
                    ['alkA','ada1','ada2','phr3','mutK','sbcCD','dcm','mutT3'])

class BacterialGenome:
    '''Once initiated with bacteria_name, 
    BacterialGenome then retrieves all the urls
    that point to the correct bacteria.   
    Public function - download_next, 
    downloads the cdna data from the next url
    and updates the index.
    '''

  
    
    url = 'ftp://ftp.ensemblgenomes.org/pub/bacteria/current/fasta'
    
    
    def _get_text(url):
        '''returns a list of text from a url 
        decoded by urrlib so that it copes with ftp sites'''
        req = urllib.request.urlopen(url)
        text = req.read().decode().split('\n')[:-1]
        req.close()
        return text

    def __init__(self,bacteria_name,directories):
        
        self.directories = directories
        self.bacteria_name = bacteria_name
        self.data_path= os.path.join(home_directory,
                                     bacteria_name,'data')
        self.paths = self._get_paths()
        self.path_index = 0
    
    def _get_paths(self):
        if 'paths.txt' in os.listdir(self.directories.basic):
            with open(os.path.join(self.directories.basic,'paths.txt')) as f:
                paths = f.read().split('\n')
            
        else:

            text1 = BacterialGenome._get_text(BacterialGenome.url)
            names = [os.path.join(BacterialGenome.url,i.split(' ')[-1][:-1]) for i in text1]
            paths = []
            print('identifying all the urls that have our bacteria in the current release')
            L = len(names)
            percentages = []
            for i,name in enumerate(names):
                text = BacterialGenome._get_text(name)
                new_paths = [os.path.join(name,i.split(' ')[-1][:-1],'cdna') for i in text]
                paths+=new_paths
                j = int(i/L*100)
                if j not in percentages:
                    print(j,' ',end = '')
                    percentages.append(j)
            random.shuffle(paths)
            with open(os.path.join(self.directories.basic,'paths.txt'),'w') as f:
                f.write('\n'.join(paths))
        
        fullnames = [i for i in paths if self.bacteria_name in i]
        return fullnames

    def _get_file(self,i=0):
        '''identifies a url from self.paths and downloads the page using wget.
        Unzips the resulting file and saves it, removing temporary files.'''
        time.sleep(5)
        if i<10:
            try:
                url = self.paths[self.path_index]
                s = subprocess.Popen(['wget',
                                      '-m',
                                      url,
                                     ])
                s.communicate()
                path = os.path.join(os.getcwd(),url.split('//')[1])
                listdir = os.listdir(path)
                if 'cdna' in listdir:
                    path = os.path.join(path,'cdna')
                listdir = os.listdir(path)
                
                name_of_file = [i for i in os.listdir(path)
                    if self.bacteria_name in i.lower()][0]
                                
                path_to_file = os.path.join(path,name_of_file)
                try:
                    with gzip.open(path_to_file) as f:
                        g = f.read()
                    name = url.split('/')[-2].lower()
                    gz_stripped_name = '{}.cdna.all.fa'\
                    .format(name)

                    self.current_file = os.path.join(
                        self.data_path,gz_stripped_name)
                    with open(self.current_file,'wb') as h:
                        h.write(g)

                except OSError as e:
                    if e.args[0] == "Not a gzipped file (b'>K')":                    
                        with open(path_to_file) as f:
                            g = f.read()
                    name = url.split('/')[-2].lower()
                    gz_stripped_name = '{}.cdna.all.fa'\
                        .format(name)

                    self.current_file = os.path.join(
                        self.data_path,gz_stripped_name)
                    with open(self.current_file,'w') as h:
                        h.write(g)
                    

                for_emptying = path[:path.index('/cdna')]
                shutil.rmtree(for_emptying)
                s.communicate()  

            except urllib.error.URLError:
                print('getting a URLError i= '.format(i))
                time.sleep(5)
                i+=1
                self._get_file(i=i)
                
    def download_next(self,verbose = False):

        self._get_file()
        self.path_index+=1

def read(path):
    with open(path) as f:
        g = f.read()
    return g

def write(path,text):
    with open(path,'w') as f:
        f.write(text)

home_directory = input(
            'pathway to storing data and results ') 
if home_directory =='':
    home_directory = '/home/skw24/Bacteria/'
        
class Directories:
    '''sets up the initial directories needed to 
    carry out the pipeline'''

    def __init__(self,bacteria_name):
        
        self.basic = home_directory
        self.root = os.path.join(self.basic,bacteria_name)
        self.data_dire = os.path.join(self.root,'data')
        self.search_dire = os.path.join(self.root,'searches')
        self.aa_dire = os.path.join(self.root,'aa_fastas1')
        self.analysis_dire = os.path.join(self.root,'analysis')
        self.linclust_dire = os.path.join(self.root,'linclust1')
        self.results_dire = os.path.join(self.root,'results')

        for path in [self.root,self.data_dire,self.search_dire,
                     self.analysis_dire,self.linclust_dire,
                     self.results_dire,self.aa_dire]:
            if not os.path.isdir(path):
                os.mkdir(path)
                
    def get_path(self):
        directory_list = os.listdir(self.data_dire)
        length = len(directory_list)
        i=0
        while i<length:
            yield os.path.join(self.data_dire,directory_list[i])
            i+=1

def download(bacteria_name,directories):

    bg = BacterialGenome(bacteria_name,directories)
    i = 0
    while len(os.listdir(directories.data_dire))<200:
        try:
            bg.download_next()
            print(i,' ', end = '')
            i+=1
        except OSError:
            i+=1
            print('download error',i)
        finally:
            if i> len(bg.paths):
                break

def space_out(x):
    '''given a string takes out the returns, replaces 
    - and then replaces
    the returns every 60 characters'''
    stripped = x.replace('\n','').replace('-','')
    length = len(stripped)//60
    return '\n'.join([stripped[i*60:(i+1)*60] for i in range(length+int(len(stripped)%60!=0))])+'\n'

def easy_linclust(file_path,dire, communicate = True):
    '''convenience function to use mmseqs easy-linclust'''
    
    if not os.path.isdir(dire):
        os.mkdir(dire)
    
    outp = os.path.join(dire,'results')
    temp = os.path.join(dire,'temp')
    p = subprocess.Popen(['mmseqs','easy-linclust',
                          file_path,outp,temp])
    if communicate:
        p.communicate()



def get_fastas(path):
    fastas = ''
    names = []
    count = 0

    with open(path) as f:
        while True:
            line = f.readline()
            if line[0]=='>':
                try:
                    name = line.split('|')[1][:-1]
                    if name not in names:
                        count+=1
                        if count<n+1:
                            names.append(name)
                        else:
                            break
                    fastas+=line    

                except IndexError:
                    print(line)
                    go = False
            else:
                fastas+=line
    return fastas,names

def translate(string):
    '''converts nucleotides to amino acids'''
    
    dic ={'AAA': 'K', 'AAC': 'N', 'AAG': 'K', 'AAT': 'N', 'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACT': 'T', 'AGA': 'R',
    'AGC': 'S', 'AGG': 'R', 'AGT': 'S', 'ATA': 'I', 'ATC': 'I', 'ATG': 'M', 'ATT': 'I', 'CAA': 'Q', 'CAC': 'H', 'CAG': 'Q',
    'CAT': 'H', 'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCT': 'P', 'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGT': 'R', 'CTA': 'L',
    'CTC': 'L', 'CTG': 'L', 'CTT': 'L', 'GAA': 'E', 'GAC': 'D', 'GAG': 'E', 'GAT': 'D', 'GCA': 'A', 'GCC': 'A', 'GCG': 'A',
    'GCT': 'A', 'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGT': 'G', 'GTA': 'V', 'GTC': 'V', 'GTG': 'V', 'GTT': 'V', 'TAA': '*',
    'TAC': 'Y', 'TAG': '*', 'TAT': 'Y', 'TCA': 'S', 'TCC': 'S', 'TCG': 'S', 'TCT': 'S', 'TGA': '*', 'TGC': 'C', 'TGG': 'W',
    'TGT': 'C', 'TTA': 'L', 'TTC': 'F', 'TTG': 'L', 'TTT': 'F'}
    a=string.upper()
    a.replace('U','T')
    a.replace('\n','')
    
    return ''.join([dic.get(a[3*i:3*(i+1)],'X') 
                    for i in range(len(a)//3)])

def aa_fasta(fasta,file):
    
    '''given a fasta splits it takes out the 
    the nucleotides translates them and provides 
    an amino acid fasta. Translate a single fastas
    
    Takes the header information from the file.'''
    
    split = fasta.split('\n')
    if '.cdna.all.fa' in file:
        file = file[:file.index('.cdna.all.fa')]
    header = file+'|'+split[0].split(' ')[0]
    seq = ''.join(split[1:])
    aa_seq = translate(seq)
    L=len(aa_seq)
    aa_sequence= '\n'.join([aa_seq[i*60:(i+1)*60] 
                            for i in range(L//60+(L%60!=0)*1)]
                          )[:-1]+'\n'
    return '>{}\n{}'.format(header,aa_sequence)


def get_aa_fastas(nuc_fastas,file):
    'translates list of nucleotide fastas'
    
    return ''.join([aa_fasta(i,file) for 
                    i in nuc_fastas.split('>')[1:]])

def translate_from_directory(directories):
    
    path_gen = directories.get_path()
    counter = 0
    while True:
        counter+=1
        try:
            path = next(path_gen)
            with open(path) as f:
                text = f.read()
            aas = get_aa_fastas(text, path.split('/')[-1])
            aa_path = path.replace('data','aa_fastas')
            with open(aa_path,'w') as f:
                f.write(aas)
            print(counter,' ', end = '')
        except StopIteration:
            break

def make_two_hundred(bacteria_name,directories):
    '''takes 200 examples from a directory of 
    sequences and translates them into a new directory.'''
    two_hundred_fa = ''
    files_for_transfer = os.listdir(directories.data_dire)
    if len(os.listdir(directories.aa_dire))<200:
        i = 0
        print('translating fastas')
    while len(os.listdir(directories.aa_dire))<200:
        bg = BacterialGenome(bacteria_name,directories)
        f = files_for_transfer[i]
        path = os.path.join(directories.data_dire,f)
        text = read(path)
        aas = get_aa_fastas(text,f)
        aa_path = path.replace('data','aa_fastas1')
        with open(aa_path,'w') as f:
            f.write(aas)
        print(i,' ', end = '')
        i+=1
        
    two_hundred_fa = ''.join([read(
        os.path.join(directories.aa_dire,i)) 
        for i in os.listdir(directories.aa_dire)])
    write(os.path.join(directories.root,'two_hundred.fa'),
          two_hundred_fa)
    print(bacteria_name, 'finished making two_hundred')



class TwoHundred:
    
    '''creates the consensus sequences from the first 
    200 bacterial strains found.In turn it: 
    translates the nucleotides to aas and saves them, 
    carries out an mmseqs linclust,
    '''
   
    
    def __init__(self,directories):


        self.large = os.path.join(directories.root,
                                  'two_hundred.fa')

        self.files = os.listdir(directories.aa_dire)[:200]
        self.paths = [os.path.join(directories.aa_dire,f) 
                      for f in self.files]
        
        nuc_paths = [os.path.join(directories.data_dire,p) 
                     for p in os.listdir(directories.data_dire)]
        #dictionary of all fastas for each strain
        self.dict = dict([(path.split('/')[-1][:-12],
                           read(path).split('>')[1:])
                          for path in nuc_paths])
        
        # dictionary of labelled fastas - identifying
        # gene and strain
        super_dict_path = os.path.join(directories.root, 
                                       'superdict.json')
        if not os.path.isfile(super_dict_path):
            value = lambda k: dict([
                (i.split('\n')[0].split(' ')[0],'\n'\
                 .join(i.split('\n')[1:])) 
                for i in self.dict[k]])
            
            self.super_dict = dict([(k,value(k)) 
                            for k in self.dict.keys()])
            with open(super_dict_path,'w') as f:
                json.dump(self.super_dict,f)
        else:
            with open(super_dict_path,'r') as f:
                self.super_dict=json.load(f)
        print('super_dict done')
        
        # cluster the translated aa fastas using linclust
        if not os.path.isfile(os.path.join(
            directories.linclust_dire,'results_cluster.tsv')):
            print('forming linclust')
            temp = os.path.join(
                directories.linclust_dire,'temp')
            if os.path.isdir(temp):
                shutil.rmtree(temp)
                
            easy_linclust(self.large,directories.linclust_dire)

        self.results_cluster = pd.read_csv(
            os.path.join(directories.linclust_dire,
                         'results_cluster.tsv'),
                                      sep = '\t',
                                      header= None)
        # save all the clusters
        self.clusters = list(
            pd.Series(self.results_cluster.groupby(by = 0)\
            .groups).map(
            lambda x: list(frozenset(self.results_cluster[1]\
                            .reindex(x)))))
        
        with open(os.path.join(directories.linclust_dire,
                               'clusters.json'),'w') as f:
            json.dump(self.clusters,f)
            
        print('clusters done')
            
        self.good_clusters = self.get_good_clusters()
            
    def get_strain_gene_from_super_dict(self,strain_gene):
        strain,gene = strain_gene.split('|')
        return self.super_dict[strain][gene]
    
    def get_good_clusters(self):
        """pick out the useful genes to use.
        Require that they exist in every strain
        and that they are between 900 and 2250 aas in 
        length.
        """
        if not os.path.isfile(os.path.join(
            directories.linclust_dire,'good_clusters.json')):
            c = pd.Series(self.clusters)
            c_shared = c[c.map(lambda x: len(x)==200)]
            c_orthologs = c_shared[
                c_shared.map(lambda x: len(set(
                    [i.split('|')[0] for i in x]))==200)]
            good_clusters = list(c_orthologs[
                c_orthologs.map(lambda x: 900< len(
                self.get_strain_gene_from_super_dict(x[0]))
                                <2250)])
            with open(os.path.join(directories.linclust_dire,
                'good_clusters.json'),'w') as f:
                json.dump(good_clusters,f)
            
        
        else:
            with open(os.path.join(directories.linclust_dire,'good_clusters.json'),'r') as f:
                good_clusters = json.load(f)
                
        return good_clusters

class Error:
    
    opener = 'w'
    num = 0
    
    def __init__(self,data,directories):
        
        path = os.path.join(directories.root,'errors.txt')
        with open(self.path,Error.opener) as f:
            f.write('error {} \n{}\n'.format(str(Error.num),data))
        
        Error.num+=1
        Error.opener = 'a'
    

class Quads:
    '''identify the quadruplet associated with
    a row given the row and cluster. The cluster is 
    a dataframe of all the sequences (nucleotides).'''
    def __init__(self,row,cluster):
        
        self.row = row
        self.string_row = ''.join(self.row)
        self.cluster = cluster

        self.places = self.get_snp_places()
        if len(self.places)>0:
            self.quad_context = list(map(self.get_quads,
                                         self.places))
        else:
            self.quad_context = []
            
    def get_snp_places(self):
        
        changes = list(self.row[self.row!= 
                                self.cluster.best].index)
        if len(changes)>1:
            nearest = list(zip(changes[1:]+[changes[0]],
            changes,[changes[-1]]+changes[:-1]))
            #take out those closer than 10bps to 
            #nearest neighbour
            snp_places =  [i[1] for i in nearest 
                    if min(abs(i[0]-i[1]),abs(i[1]-i[2]))>9]
            if 0 in snp_places:
                snp_places.remove(0)
            last = self.row.size
            if last in snp_places:
                snp_places.remove(last)
            return snp_places
        else:
            return []
    
    def get_quads(self,i):
        if 0<i<self.row.shape[0]-1:
            #snp is the mutant triplet
            snp = self.row[i-1]+self.cluster.best[i]\
                +self.row[i]+self.row[i+1]
            codon = self.string_row[i-2:i+1]
            #best_codon is the assumed wild codon
            best_codon = self.cluster.string_best[i-2:i+1]
            return [snp,codon,best_codon,i%3]
        else:
            return ['','','',-1]

class Cluster:
    '''a cluster is a cluster of genes identified by linclust. 
    Properties:
        cluster: list of strain/genes
        data: data for aligning
        good_seqs: dataframe of aligned sequences- removing any indels before the start of the consensus.
        best: the consensus sequence
        string_best: the consensus sequence as a string
        
    Methods:
        quads_not_reduced - returns list of the quads for snps 
    
        count_background(self): returns tuple: 
            all triplets in the consensus sequence;
            just those that could be mutated with fourfold redundancy;
            codon pluses = all codons plus the 3' flanking nucleotide;

    '''
    
    
        
    def __init__(self,cluster,two_hundred,directories):
        
        self.directories = directories
        self.cluster = cluster   
        self.two_hundred = two_hundred
        self.data = self.get_clustalo_data()
        x=time.time()
        fastas = self.clustalo()
        self.names = [i.split('|')[0] for i in fastas]

        y = time.time()-x
        self.seqs = pd.DataFrame(
            [list(''.join(i.split('\n')[1:])) for i in fastas])
        best = self.seqs.loc['best_seq'] = self.seqs.apply(
            lambda col:col.value_counts().index[0])
               
        #take out any indels at the start
        try:
            start = ''.join(best).index('ATG')
        except ValueError:
            print('ATG not present \n'.format(self.cluster))
            start = 0
            
        self.good_seqs = self.seqs.iloc[:,start:]
        self.good_seqs.index = self.names+['best_seq']
        self.good_seqs.columns = range(self.good_seqs.shape[1])
        self.best = self.good_seqs.loc['best_seq']
        self.string_best = ''.join(self.best)
        
        
        s = self.seqs.apply(lambda row: 
                            (row == self.seqs.iloc[-1])\
                            .astype(int) ,axis = 1)
        self.seq_id_series = (s.T.sum()/s.shape[1])[:-1]
        self.similar = self.seq_id_series[
            self.seq_id_series>0.99].index
        self.num_similar = self.similar.shape[0]
        self.all_names = pd.Series(self.cluster).map(
            lambda x:x.split('|')[0])
        self.names = list(set(self.all_names[self.similar]))
        
    def get_clustalo_data(self):
        '''a list of 
        strain|genes that were identified by linclust 
        as being related'''
        for_clustalo = ''
        for j in self.cluster:
            strain,gene = j.split('|')
            if strain in self.two_hundred.super_dict.keys():
                if gene in \
                self.two_hundred.super_dict[strain].keys():
                    for_clustalo+='>'+j+'\n'+self.two_hundred.super_dict[strain][gene]
        return for_clustalo

    def clustalo(self):
        try:
            r = random.randint(100000,999999)

            temp = os.path.join(home_directory,bacteria_name,
                   'temp{}.fa'.format(str(r)))
            temp1 = os.path.join(home_directory,bacteria_name,
                   'temp{}.fa'.format(str(r+1)))
            with open(temp,'w') as f:
                    f.write(self.data)
            #CALL CLUSTALO - CHECK THIS IS WHERE
            #YOU SAVED THE PROGRAM
            try:
                p = subprocess.Popen(['clustalo',
                                      '--force',
                                      '-i',temp,
                                      '-o',temp1])
                p.communicate()
            except:
                print('''Unable to complete
                Have you installed clustalo in the 
                home directory? If not you will
                need to change the address in line 582
                ''')
            with open(temp1,'r') as f:
                fastas = f.read().split('>')[1:]
            os.remove(temp)
            os.remove(temp1)
            return fastas

        except (OSError,FileNotFoundError) as e:
            print(e)
            data = str(e)+'\n'+self.data
            Error(data,self.directories)          
            return []
     
    def quads_not_reduced(self):
        return self.good_seqs.apply(lambda row: 
                        Quads(row,self).quad_context,
                        axis = 1)
    
    def count_background(self):
        '''return
            codon pluses = all codons plus 
            the 3' flanking nucleotide
            '''
        length = len(self.string_best)//3-1
        codon_pluses = [self.string_best[3*i:3*i+4] 
                        for i in range(length)]
        return codon_pluses
    

    def get_uniques(self):
        '''find the dictionary of silent quadruplets 
        that are unique - ie only one of the strains 
        has been mutated
        '''
        #initialise
        unique_strain_quad = []
        changed_quads = pd.Series(0,index = quadruplets_sorted)

        width = self.good_seqs.shape[1]
        
        for i in range(1,width//3):
            j=3*i #iterate over the 2nd registers
            if j<width-1: #dont bother with the edges - no good quads
                #check that you're not in a patch of indels
                if set(self.good_seqs.loc['best_seq'][[j-2,j-1,j,j+1]])-set('ACGT')==set():
                    vc= self.good_seqs.iloc[:,j].value_counts()
                    if 1 in set(vc.values):
                        um = vc[vc==1].index  #if any of the snps are unique    
                        for m in um:#iterated through unique snps
                            unique_strain = self.good_seqs[j]\
                            .loc[self.good_seqs[j]==m].index[0]
                            snippet = self.good_seqs.loc[
                                [unique_strain,'best_seq']]\
                                [[j-2,j-1,j,j+1]]
                            
                            silentw =translate(''.join(
                                snippet.iloc[0,:-1]))
                            
                            silentm =translate(''.join(
                                snippet.iloc[1,:-1]))
                            is_silent = silentw==silentm
                            
                            if is_silent:
                
                                quad = ct(''.join(
                                    [snippet.iloc[i,j] for i,j in [(1,1),(0,2),(1,2),(1,3)]]))
                                
                                if snippet.iloc[0,2] in set('ACGT'):#check not a deletion
                                    unique_strain_quad.append(
                                        [unique_strain,quad])
                                    changed_quads[quad]+=1

        return unique_strain_quad,changed_quads

 
                           

def cluster_genes(two_hundred,directories):
    cluster_info = []
    for i,cluster in enumerate(two_hundred.good_clusters):
        try:
            self = Cluster(cluster,two_hundred,directories)
            cluster_info.append(self)
        except ValueError as e:
            print(e)
            Error('{}\ncluster_number{}\n{}'.format(str(e),i,'\n'.join(cluster)))
            
    return cluster_info

def cluster_strains(cluster_info, color_threshold = ''):
    similarities = []
    for self in cluster_info:
        self.seq_id_series.index  =self.all_names
        similarities.append(self.seq_id_series)

    similarities_df = pd.concat(similarities,axis = 1,sort=True)
    similarities_df.to_csv(os.path.join(directories.root,'results','similarities.csv'))
    distances = (1-similarities_df).fillna(1)
    Z = linkage(distances,method = 'ward')
    fig,ax = plt.subplots(figsize = (7,3))
    if color_threshold == '':
        den = dendrogram(Z,ax=ax, get_leaves = True, 
                         no_labels = True)
    else:
        den = dendrogram(Z,ax=ax, get_leaves = True, 
                         no_labels = True, 
                         color_threshold = color_threshold)
    path = os.path.join(directories.root,
                        'results',
                    'clustering_{}_by_sequence_identity.pdf'\
                        .format(bacteria_name))
    fig.savefig(path)
    
    s = pd.Series(dict(zip(den['leaves'],den['color_list'])))
    name_colours_df = pd.concat([pd.Series(distances.index),
                                 s.reindex(range(200))],axis=1)
    strain_clusters = pd.Series(name_colours_df.groupby(
        by = 1).groups).map(lambda x: list(
        name_colours_df.reindex(x)[0]))
    numbers = strain_clusters.map(len)
    
    return Z,den,fig,strain_clusters,numbers

class StrainCluster:
    """given selected strains pulls out all the genes 
    that are shared by
    the strains, then finds all the USSs and 
    provides the quadruplet information about them
    to build up a mutational fingerprint for the strains
    in the cluster. This takes a long time to complete
    
    """
    def __init__(self,selected_strains,two_hundred,
                 label,directories,cutoff = 20,show_freq = 10):
        
        self.text = ''
        self.selected_strains = selected_strains
        self.two_hundred=two_hundred
        self.label = label
        self.directories = directories
        self.cutoff = cutoff
        self.show_freq = show_freq
        
        self.shared_clusters = self.get_shared_clusters()
        self.length = len(self.shared_clusters)

        
        self._unique_strain_quads = []
        self._unique_quads = pd.Series(0,
                            index = quadruplets_sorted)
        self._all_quads_not_reduced = pd.Series(
            index = self.selected_strains).map(lambda x:[])
        self._codon_pluses = []
       
    def res_path(self,x):
        return os.path.join(directories.root,
                    'results','{}_{}'.format(self.label,x))

    def get_shared_clusters(self):
        '''filters two_hundred.clusters for 
        the selected_strains, pull out any with paralogues, 
        and returns
        gene clusters that have all the selected 
        strains in them'''

        path = self.res_path('shared_clusters_large.json')
        
        if not os.path.isfile(path):
            self.text+='finding shared clusters'
     
            in_selected = lambda cl: [i for i in cl 
                    if i.split('|')[0] in self.selected_strains]
            clusters_in_selected = pd.Series(
                self.two_hundred.clusters).map(in_selected)

            def check_no_paralogues(cl):
                names = [i.split('|')[0] for i in cl]
                return len(names)==len(set(names))

            no_paralogues = clusters_in_selected[
                clusters_in_selected.map(check_no_paralogues)]
            lengths = no_paralogues.map(len)
            shared_non_paralogous_cluster_index = lengths[
                lengths==len(self.selected_strains)].index
            shared_clusters= list(clusters_in_selected.reindex(shared_non_paralogous_cluster_index))
            with open(path,'w') as f:
                json.dump(shared_clusters,f)
                 
        else:
            with open(path,'r') as f:
                shared_clusters = json.load(f)
                
        return shared_clusters         
    
    def build_quads(self,cl):
        cluster= Cluster(cl,self.two_hundred,self.directories)
        usq,q=cluster.get_uniques()
        self._unique_strain_quads+=usq
        self._unique_quads+=q
        self._all_quads_not_reduced+=cluster.quads_not_reduced()
        self._codon_pluses+=cluster.count_background()
        
    def count(self,strain):

        def flatten(s):
            r = s.copy()
            df = pd.DataFrame(list(r.index.map(list)),
                              columns = r.index.names)
            r.index = range(r.shape[0])
            df['values'] = r
            return df
        
        try:
            df = pd.DataFrame(self._all_quads_not_reduced[strain],columns = ['snp','codon','best_codon','register'])
            df['corrected'] = df['snp'].map(ct)
            df = df.loc[df['corrected'].map(lambda x:
                                            x in all_quads)]
            df = flatten(df.groupby(df.columns.tolist(),as_index=False).size())
            four_fold_redundancy = frozenset([i+j for i in ['AC','GT','GG','GC','TC','CT','CC','CG'] for j in 'ACGT'])

            df['silent'] = df.apply(lambda row: 
                (translate(row['codon'])==translate(row['best_codon']))
                and (row['register']==2),
                axis=1)

            df['four_fold'] = df.apply(lambda row: 
                (row['codon'] in four_fold_redundancy) and 
                (row['register']==2),axis=1)
        
            df['name'] = strain
        except ValueError:
            df = pd.DataFrame(columns = ['snp','codon',
                                'best_codon','register',
                                'corrected','silent','four_fold','name'])
        
        
        return df

    def get_quads(self):
        '''The meat of the class. This function iterates 
        through shared genes, calling build quads
        which clusters them and finds all the snps as well 
        as just the unique ones. It also finds the distribution
        of possible silent snps'''
        
        path = self.res_path('all_quads_counted.csv')

        #build up all the snps and silent background
        for i,cl in enumerate(self.shared_clusters):
            self.build_quads(cl)
            print(i,' ', end = '')
            if (self.show_freq>0) and (i%self.show_freq==0):
                plot_quads(self._unique_quads)
                plt.show()

        #put information into appropriate format and save it
        self._unique_quads.to_csv(self.res_path(
            'unique_quads.csv'))

        unique_strain_quads = pd.DataFrame(self._unique_strain_quads)
        unique_strain_quads.columns = ['strain','quads']

        count_quads_sorted = lambda x,df: df['quads'].reindex(x).value_counts().reindex(
            quadruplets_sorted).fillna(0).astype(int)

        by_strain = pd.Series(unique_strain_quads.groupby(by = 'strain').groups).map(
            lambda x: count_quads_sorted(x,unique_strain_quads))
        by_strain_df = pd.concat(list(by_strain),axis = 1)
        by_strain_df.columns = by_strain.index
        by_strain_df.to_csv(self.res_path('{}_{}_unique_mutations.csv'.format(bacteria_name,
                                                                                        self.label)))

        all_quads_counted = pd.concat(
            list(self._all_quads_not_reduced.index.map(
                self.count)),sort = True)
        all_quads_counted.to_csv(path)   

        with open(self.res_path('codon_pluses.json'),'w') as f:
            json.dump(self._codon_pluses,f)

        return    all_quads_counted,by_strain_df

    
    def group_values(self):
        '''identify all the silent quads'''
        path = self.res_path('grouped_values.csv')
        silent_quads = self.all_quads_counted.loc[
            self.all_quads_counted['silent']]
        silent_quads.index = range(silent_quads.shape[0])
        groups = pd.Series(silent_quads.groupby(by = 'name').groups)

        def make_96(x):
            reindexed = silent_quads.reindex(x)
            series= pd.Series(reindexed['values'].values,index = reindexed['corrected'].values).groupby(level = 0).sum()

            return series.reindex(quadruplets_sorted).fillna(0).astype(int)

        grouped_values = pd.concat(list(groups.map(make_96)),axis = 1)
        grouped_values.columns = groups.index
        grouped_values.to_csv(path)
        return grouped_values
    
 
    def silent_distribution(self):
        '''A codon_plus is the four nucleotides making up a codon and the final nucleotide which flanks the codon.
        This is sufficient information to identify the silent possible quads that could result from a mutation in 
        the 2nd register.

        Input : a list of codon_pluses generated by looking at a nucleotide sequence;
        Output: a series - the distribution of potential silent quadruplets, ordered by mutation first and
        then the flanking nucleotides. '''
        path = self.res_path('distribution_of_random_silent_mutations.csv')

        class SilentCounts:
            silent_quad_counts = {}
            with open('silent_quads.json','r') as f:
                silent_quads = json.load(f)

            def __init__(self,codonplus):
                new_quads = SilentCounts.silent_quads[codonplus]
                value = codon_plus_value_counts[codonplus]
                for quad in new_quads:
                    SilentCounts.silent_quad_counts[quad]=SilentCounts.silent_quad_counts.get(quad,0)+value

        codon_plus_value_counts_rough = pd.Series(self._codon_pluses).value_counts()
        codon_plus_value_counts = codon_plus_value_counts_rough.reindex(
            [i for i in codon_plus_value_counts_rough.index if '-' not in i])

        codon_plus_value_counts.index.map(SilentCounts);

        silent_possibilities_rough =  pd.Series(SilentCounts.silent_quad_counts)
        silent_possibilities_rough.index = silent_possibilities_rough.index.map(ct)
        silent_possibilities = silent_possibilities_rough.groupby(level = 0).sum()[
            quadruplets_sorted].fillna(0).astype(int)
        silent_possibilities.to_csv(path,header  = False)
        return silent_possibilities

    def get_results(self):
        
        path = self.res_path('results.csv')
        
        if not os.path.isfile(path):
            normalised_snps = self.silent_snps/self.silent_snps.sum()
            normalised_possibilities = self.silent_possibilities/self.silent_possibilities.sum()
            mutation_rates = (normalised_snps/normalised_possibilities).fillna(1)
            results = pd.concat([normalised_snps,normalised_possibilities,mutation_rates],axis=1)
            results.columns = ['normalised_snps','normalised_possibilities','mutation_rates']
            results.to_csv(path)
        else:
            results = pd.read_csv(path, index_col = 0)
        
        return results
    
    def show(self):
        fig,axarr = plt.subplots(3,figsize = (7,9))
        for i in range(3):
            col = self.results.columns[i]
            ax = axarr[i]
            plot_quads(self.results[col],ax)
            ax.set_title(col.replace('_',' '));
        plt.tight_layout()
        fig.savefig(self.res_path('results.pdf'))
        
    def get_unique_freqs(self):
        silent_dist = normalise(self.silent_possibilities)
        unique_freqs = self.by_strain_df.apply(lambda col: col/silent_dist).fillna(1)
        fig,ax = plt.subplots()
        plot_quads(normalise(unique_freqs.copy().T.sum()),ax =ax)
        fig.savefig(self.res_path('unique_frequencies.pdf'))
        unique_freqs.to_csv(self.res_path('unique_frequences.csv'))
        return unique_freqs

    def cluster_highly_mutated_uniques(self):
        highly_mutated = self.by_strain_df.T.loc[self.by_strain_df.sum()>self.cutoff].index
        highly_mutated_freqs = self.unique_freqs[highly_mutated]
        if highly_mutated_freqs.shape[0]>0:

            distances = pd.DataFrame(1-cosine_similarity(highly_mutated_freqs.T,highly_mutated_freqs.T))
            length = distances.shape[0]
            if distances.sum().sum()>0:
                columns = pd.Series(highly_mutated)

                for i in range(length):
                    distances.iat[i,i]=0
                    for j in range(i+1,length):
                        distances.iat[i,j] = distances.iat[j,i]

                link = linkage(ssd.squareform(distances))

                fig,ax = plt.subplots(figsize = (10,4))
                Z = dendrogram(link,ax = ax,orientation = 'right')

                name = '_'.join(columns[0].split('_')[:2])

                shortening = ''.join([i[0] for i in bacteria_name.split('_')])

                strains =[i.replace(name,shortening) for i in columns.reindex(Z['leaves']).values]

                label = ' '.join(bacteria_name.split('_')[:2])

                ax.set_yticklabels(strains)
                ax.set_title(label)
                plt.tight_layout()

                fig.savefig(self.res_path('clustered_highly_mutated_strains.pdf'.format(bacteria_name,
                                                                                                  self.label)))
                path = os.path.join(home_directory,
                                    'results',
                   '{}_{}_clustered_highly_mutated_strains.pdf'\
                        .format(
                        bacteria_name,self.label))
                fig.savefig(path)
        return highly_mutated_freqs

    def find_sigs(self, n_components = 5):
        """converts the mutational fingerprints into 
        normalised mutational signatures"""
        normalised_unique_freqs = self.highly_mutated_freqs\
            .apply(normalise)

        V = normalised_unique_freqs.values
        W0,H0,_ = nmf(V.T,n_components = n_components,
                      init = 'nndsvd')

        H = pd.DataFrame(H0.T,index = quadruplets_sorted)

        Hsizes = H.apply(lambda col: sqrt((col**2).sum()))

        W = pd.DataFrame(W0)

        W*=Hsizes

        H/=Hsizes

        fig1,axarr = plt.subplots(4,figsize = (7,10))
        plot_quads(normalised_unique_freqs.T.mean(),
                   ax = axarr[0])
        axarr[0].set_title('{} frequency unique mutations'.format(bacteria_name))
        plot_quads(pd.Series(H[0],index = quadruplets_sorted),ax = axarr[1])
        axarr[1].set_title('signature 0')
        plot_quads(pd.Series(H[1],index = quadruplets_sorted),ax = axarr[2])
        axarr[2].set_title('signature 1')
        pd.DataFrame(W).plot(kind = 'bar',ax = axarr[3])
        axarr[3].set_title('breakdown of highly mutated strains by signature')
        plt.tight_layout()

        fig1.savefig(self.res_path('unique_signatures.pdf'))
        fig1.savefig(os.path.join(home_directory,'results',
            '{}_{}_unique_signatures.pdf'.format(bacteria_name,
                                                 self.label)))
        return normalised_unique_freqs

    def get_info(self):
        self.all_quads_counted,self.by_strain_df = self.get_quads()
        self.silent_possibilities = self.silent_distribution()
        self.grouped_values = self.group_values()
        self.unique_freqs = self.get_unique_freqs()
        self.highly_mutated_freqs = self.cluster_highly_mutated_uniques()
        self.normalised_unique_freqs = self.find_sigs()